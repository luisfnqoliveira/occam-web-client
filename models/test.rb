class Occam
  class Test
    attr_reader :name
    attr_reader :scenario
    attr_reader :it
    attr_reader :class
    attr_reader :type
    attr_reader :time
    attr_reader :assertions

    def initialize(options)
      @name       = options[:name]
      @it         = options[:it]
      @scenario   = options[:scenario]
      @class      = options[:class]
      @type       = options[:type]
      @time       = options[:time]
      @assertions = options[:assertions]
    end

    def header
      self.name || self.scenario || self.it
    end
  end
end
