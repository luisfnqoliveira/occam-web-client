# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Layout
    # This model handles rendering information when displaying any page.

    # This describes the current rendering mode. Values include:
    #   :desktop  => A full rendering of the page
    #   :mobile   => Intended to render to a small device
    #   :embedded => Intended to render to an embedded container
    attr_reader :view

    # This describes the pagination on the current page.
    attr_reader :pagination

    # This denotes which tab on the main tab strip we are viewing.
    attr_reader :tab

    # This tells us which help option has been opened.
    #
    # Although the help bubbles on the site are generally opened via javascript,
    # there are times when they must be opened by a full page load when js is
    # disabled. This will help trigger those help bubbles on a render.
    attr_reader :help

    def initialize(options)
      @view       = options[:view] || :desktop
      @pagination = options[:pagination]
      @tab        = options[:tab]
      @help       = options[:help]
    end
  end
end
