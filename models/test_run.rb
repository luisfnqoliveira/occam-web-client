class Occam
  class TestRun
    def initialize(options = {})
      @file   = options[:file]
    end

    def parse
      if !defined?(@data)
        if @file
          begin
            f = File.open(@file, "r")
            @data = JSON.parse(f.read(), :symbolize_names => true)
            f.close
          rescue
          end
        end
      end

      @data
    end

    def groups
      if !defined?(@groups) and self.parse
        @groups = {}
        self.parse[:groups].each do |key, group|
          @groups[key] = Occam::TestGroup.new(group)
        end
      end

      @groups || {}
    end

    def group(name)
      self.groups[name]
    end

    def status
      (self.parse || {})[:status]
    end

    def metadata
      (self.parse || {})[:metadata] || {}
    end

    def statistics
      (self.parse || {})[:statistics] || {}
    end

    def timings
      (self.parse || {})[:timings]
    end

    def passes
      (self.parse || {})[:passes] || []
    end

    def failures
      (self.parse || {})[:fails] || []
    end

    def skips
      (self.parse || {})[:skips] || []
    end

    def errors
      (self.parse || {})[:errors] || []
    end

    def all
      if !defined?(@all)
        @all = self.passes + self.failures + self.skips + self.errors
      end

      @all
    end
  end
end
