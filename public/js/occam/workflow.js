/* This file handles the interactivity of workflows.
 */

// TODO: accept a lack of a 'position' in imported json
// TODO: accept a lack of 'inputs' and 'outputs'

// Initialize any Workflows on the current page
window.addEventListener("load", function(event) {
  var workflows = $('.content occam-workflow');

  workflows.each(function(index) {
    var workflow = new Occam.Workflow($(this), index);
  });
});

var initOccamWorkflow = function(Occam) {
  var Workflow = Occam.Workflow = function(element, index) {
    var self = this;

    // Initialize events
    self.events = {};

    self.element = element;
    self.element.css('max-height', 'none');

    // Look for a run id
    self.runID = self.element[0].getAttribute("data-run-id");

    if (self.runID) {
      // This represents a run
      // The workflow should not be editable
      // And we need to get the run information
      self.runObject = new Occam.Object(self.element[0].getAttribute("data-run-object-id"),
                                    self.element[0].getAttribute("data-run-object-revision"));
      self.runData(self.runObject, self.runID, function(data) {
        // Create a polling timer for updating the run (if it is not finished/failed)
        if (!data.run.failureTime && !data.run.finishTime) {
          self.pollRunTimer = window.setInterval(function() {
            // Poll the run info and pass it to the workflow
            self.pollRun();
          }, Workflow.POLL_TIME);
        }

        self.initializeWidget(data.nodes);
      });
    }
    else {
      self.initializeWidget();
    }
  };

  /* The time in milliseconds between polling for updates in a run.
   */

  Workflow.POLL_TIME = 2000;

  /* Fires a callback for the given event.
   */
  Workflow.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  Workflow.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  Workflow.prototype.pollRun = function() {
    var self = this;

    self.runData(self.runObject, self.runID, function(data) {
      // Pass along job details to workflow
      self.workflow.updateJobs(data.nodes);

      if (data.run.finishTime || data.run.failureTime) {
        window.clearInterval(self.pollRunTimer);
        self.trigger('done', data);
      }
    });
  };

  Workflow.prototype.runData = function(object, runID, callback) {
    Occam.Util.get(object.url({"path": "runs/" + runID}), function(data) {
      callback(data);
    }, "json");
  };

  Workflow.prototype.initializeWidget = function(jobs) {
    var self = this;

    self.workflow = new window.Workflow(self.element[0], jobs);
    var sidebar = self.element.find(".sidebar li.connection")[0];
    if (sidebar) {
      var selectionNode = window.Workflow.Node.createFor(sidebar, self.workflow);

      var empty = self.element.find('.input.start').length == 1;

      self.sidebar = self.element.find('.sidebar:not(.right)');
      self.sidebar2 = self.element.find('.sidebar.right');

      var blah = self.sidebar[0].querySelector('h2:first-child');
      blah.addEventListener('click', function(event) {
        experiment_data=self.exportJSON();
        split_url = $("div.content h1 div.name span a")[0].getAttribute("href").split("?")
        url = split_url[0] + "/0/files/data.json";
        if (split_url[1])
        {
          url += "?" + split_url[1];
        }
        data={}
        data["data"]=experiment_data
        $.post( url , data, function(data){
          window.location.replace(data["url"])
          }, 'json'  );
      });

      self.element.appear();
      self.element.one('appear', function() {
        self.workflow.redraw();
        selectionNode.arrangeOutputWires();
        selectionNode.arrangeInputWires();
        selectionNode.repositionInputs();
      });
    }

    self.workflow.on("button-click", function(event) {
      if (event.element.classList.contains("configure-button")) {
      }
      else if (event.element.classList.contains("view-button")) {
        var objectURL = "";
        objectURL += "/" + event.node.element.getAttribute("data-object-id");
        objectURL += "/" + event.node.element.getAttribute("data-object-revision");
        window.open(objectURL,'_blank');
      }
    });

    self.workflow.on("node-removed", function(event) {
      var node = event.node;
      var element = event.element;

      var index = parseInt(element.getAttribute('data-index'));

      // Remove configuration tab
      self.configurationTabs.removeTab(index);
    });

    self.connections = self.element.children("ul.connections").first();
    self.draggable = self.connections;

    if (self.element[0].classList.contains("editable")) {
      var blah = self.sidebar[0].querySelector('h2:first-child');
      blah.addEventListener('click', function(event) {
        self.exportJSON();
      });

      self.connectToConfigurationTabs();
      self.initializeSidebar();
      self.applySidebarEvents();
      self.applySidebarNodeEvents(self.sidebar.find("li.connection"));
    }

    // Load from the experiment
    Occam.object = Occam.object || new Occam.Object();
    if (Occam.object && Occam.object.type == "experiment") {
      Occam.object.objectInfo(function(info) {
        (info.contains || []).forEach(function(item) {
          if (item.type == "workflow") {
            self.loadFrom(new Occam.Object(item.id, item.revision, item.type));
          }
        });
      });
    }
  };

  /* This function gets references to the tab strip for configurations.
   */
  Workflow.prototype.connectToConfigurationTabs = function() {
    var tabElement = this.element.parent().next().children('.tabs');
    this.configurationTabs = Occam.Tabs.load(tabElement[0]);
  };

  /* This function sets up the dynamic interactions with the sidebar.
   */
  Workflow.prototype.initializeSidebar = function() {
    var self = this;
    var collapseBars = this.element.parent().find('.collapse');

    collapseBars.on('click', function(event) {
      var collapse = $(this);

      collapse.css({ 'display': 'none' });
      collapse.toggleClass('reveal');

      var sidebar = self.sidebar;

      if (collapse.hasClass("right")) {
        sidebar = self.sidebar2;
      }

      if (sidebar[0].hasAttribute('hidden')) {
        // Slide the sidebar to the right to show it
        sidebar[0].removeAttribute('hidden');
        sidebar.animate({
          'width': 320
        }, 200, function() {
          if (collapse.hasClass("right")) {
            collapse.css({
              'right': '320px',
              'display': ''
            });
          }
          else {
            collapse.css({
              'left': '320px',
              'display': ''
            });
          }
        });
      }
      else {
        // Slide the sidebar to the left and hide it
        sidebar.animate({
          'width': 0
        }, 200, function() {
          sidebar[0].setAttribute('hidden', true);

          if (collapse.hasClass("right")) {
            collapse.css({
              'right': '0px',
              'display': ''
            });
          }
          else {
            collapse.css({
              'left': '0px',
              'display': ''
            });
          }
        });
      }
    });
  };

  /* This function will set up the events to attach the sidebar attach
   * button to the current input box.
   */
  Workflow.prototype.applySidebarEvents = function(newActiveBox) {
    var self = this;
    if (newActiveBox !== undefined) {
      this.sidebar.find('.no-input')[0].setAttribute('hidden', true);
      this.sidebar.find('.selection')[0].removeAttribute('hidden');

      var collapse = this.element.children('.collapse:not(.right)');
      if (collapse.hasClass('reveal')) {
        collapse.trigger('click');
      }

      collapse = this.element.children('.collapse.right');
      if (collapse.hasClass('reveal')) {
        collapse.trigger('click');
      }
    }

    // General object autocomplete
    var autoCompleteType = Occam.AutoComplete.load(self.sidebar.find('.auto-complete[name=type]')[0]);
    var autoCompleteElement = self.sidebar.find('.auto-complete[name=name]');
    var autoComplete = Occam.AutoComplete.load(autoCompleteElement[0]);
    autoCompleteType.on("change", function(event) {
      autoComplete.clear();
    });

    autoComplete.on("change", function(event) {
      var object = {}
      var hidden            = autoCompleteElement.parent().children('input[name=object-id]');
      object["object_type"] = autoCompleteElement[0].getAttribute('data-object_type');
      object["revision"]    = autoCompleteElement[0].getAttribute('data-revision');
      object["name"]        = autoCompleteElement.val();
      object["uid"]         = hidden.val();

      // Remove input/output pins
      var nodes = self.sidebar.find("li.connection");

      nodes.children(".name").text(object["name"]);
      nodes.children(".type").text(object["object_type"]);
      nodes.children(".icon")[0].setAttribute('data-object-type', object["object_type"]);

      var node = nodes[0];

      node.setAttribute("data-object-revision", object["revision"]);
      node.setAttribute("data-object-id", object["uid"]);

      var inputs  = node.querySelector("ul.inputs");
      var outputs = node.querySelector("ul.outputs");

      var pins = inputs.querySelectorAll("li.input");
      if (pins) {
        pins.forEach(function(pin) {
          pin.remove();
        });
      }
      pins = outputs.querySelectorAll("li.output");
      if (pins) {
        pins.forEach(function(pin) {
          pin.remove();
        });
      }

      // Load input/output pins
      var realized = new Occam.Object(object["uid"], object["revision"], object["object_type"]);
      self.currentObject = realized;
      if(object["object_type"] === "workflow") {
        realized.objectInfo(function(info) {
          if (info.file === undefined) {return;}
          realized.retrieveJSON(info.file, function(data) {
            let inputCounter = 0;
            let outputCounter = 0;
            let selfOutput={
              "name": "self",
              "type": info.type
            };
            // TODO: Refactor this! A lot of repeated code
            // ADD self ////////////////////////////////////////////
            var pin = document.createElement("li");
            pin.classList.add("output");
            pin.setAttribute("data-index", outputCounter);

            var label = document.createElement("div");
            label.classList.add("label");

            var name = document.createElement("span");
            name.classList.add("name");
            name.innerHTML = selfOutput.name || "output";

            var type = document.createElement("span");
            type.classList.add("type");
            type.innerHTML = selfOutput.type || "";

            label.appendChild(type);
            label.appendChild(name);

            pin.appendChild(label);

            outputs.appendChild(pin);
            outputCounter++;

            ///////////////////////////////////////////////////
            (data.connections || []).forEach(function (nodee) {
              if (nodee.inputs) {
                nodee.inputs.forEach(function(input) {
                  if((input.connections||[]).length !== 0) {return;}
                  if(input.type === "configuration") {return;}
                  var pin = document.createElement("li");
                  pin.classList.add("input");
                  pin.setAttribute("data-index", inputCounter);

                  var label = document.createElement("div");
                  label.classList.add("label");

                  var name = document.createElement("span");
                  name.classList.add("name");
                  name.innerHTML = input.name || "input";

                  var type = document.createElement("span");
                  type.classList.add("type");
                  type.innerHTML = input.type || "";

                  label.appendChild(type);
                  label.appendChild(name);

                  pin.appendChild(label);

                  inputs.appendChild(pin);
                  inputCounter++;
                });
              }
              if (nodee.outputs) {
                nodee.outputs.forEach(function(output) {
                  if((output.connections||[]).length !== 0) {return;}
                  var pin = document.createElement("li");
                  pin.classList.add("output");
                  pin.setAttribute("data-index", outputCounter);

                  var label = document.createElement("div");
                  label.classList.add("label");

                  var name = document.createElement("span");
                  name.classList.add("name");
                  name.innerHTML = output.name || "output";

                  var type = document.createElement("span");
                  type.classList.add("type");
                  type.innerHTML = output.type || "";

                  label.appendChild(type);
                  label.appendChild(name);

                  pin.appendChild(label);

                  outputs.appendChild(pin);
                  outputCounter++;
                });
              }
            });
            // Redraw node
            var workflowNode = window.Workflow.Node.createFor(node, self.workflow);
            workflowNode.redraw();
          });
        });
      }else{
        realized.objectInfo(function(data) {
          if (data.inputs) {
            data.inputs.forEach(function(input, i) {
              var pin = document.createElement("li");
              pin.classList.add("input");
              pin.setAttribute("data-index", i);

              if (input.type == "configuration") {
                pin.classList.add("hidden");
                pin.setAttribute("hidden", true);
              }

              if (input.max) {
                pin.setAttribute("data-max", input.max);
              }

              var label = document.createElement("div");
              label.classList.add("label");

              var name = document.createElement("span");
              name.classList.add("name");
              name.innerHTML = input.name || "input";

              var type = document.createElement("span");
              type.classList.add("type");
              type.innerHTML = input.type || "";

              label.appendChild(type);
              label.appendChild(name);

              pin.appendChild(label);

              inputs.appendChild(pin);
            });
          }
          data.outputs = (data.outputs || []);
          data.outputs.unshift({
            "name": "self",
            "type": data.type
          });
          if (data.outputs) {
            data.outputs.forEach(function(output, i) {
              var pin = document.createElement("li");
              pin.classList.add("output");
              pin.setAttribute("data-index", i);

              var label = document.createElement("div");
              label.classList.add("label");

              if (i == 0 && data.outputs.length > 1) {
                pin.classList.add("hidden");
                pin.setAttribute("hidden", true);
              }

              var name = document.createElement("span");
              name.classList.add("name");
              name.innerHTML = output.name || "output";

              var type = document.createElement("span");
              type.classList.add("type");
              type.innerHTML = output.type || "";

              label.appendChild(type);
              label.appendChild(name);

              pin.appendChild(label);

              outputs.appendChild(pin);
            });
          }
          // Redraw node
          var workflowNode = window.Workflow.Node.createFor(node, self.workflow);
          workflowNode.redraw();
        });
      }
    });

    this.sidebar.find('.button[type=submit]').on('click', function(event) {
      if (newActiveBox) {
        var object = {}
        var autoComplete = self.sidebar.find('.auto-complete[name=name]');
        var hidden       = autoComplete.parent().children('.hidden[name=object-id]');
        object["object_type"] = autoComplete.data('object_type');
        object["revision"]    = autoComplete.data('revision');
        object["name"]        = autoComplete.val();
        object["uid"]         = hidden.val();

        /* Set fields to reflect choice */
        newActiveBox.find('form input.object-type').val(object["object_type"]);
        newActiveBox.find('form input.object-name').val(object["name"]);
        /* Set hidden field to: object['uid'] */
        newActiveBox.find('form input.object-id').val(object["uid"]);
        newActiveBox.find('form input.object-revision').val(object["revision"]);
        newActiveBox.find('form input.button').removeAttr('disabled', '');

        newActiveBox.find('form input.object-type').css({
          "background-image": autoComplete.css('background-image')
        });

      }
      event.stopPropagation();
      event.preventDefault();
    });
  };

  Workflow.prototype.loadFrom = function(obj) {
    var self = this;
    obj.objectInfo(function(info) {
      if (info.file === undefined) {
        return;
      }
      obj.retrieveJSON(info.file, function(data) {
        // Get workflow pan position
        var width  = self.workflow.element.offsetWidth;
        var height = self.workflow.element.offsetHeight;

        data.center = data.center || {"x": 0, "y": 0};

        // Center the workflow diagram upon the stored point
        // (The workflow's viewport position is the top left coordinate)
        workflowLeft = data.center.x + (width  / 2);
        workflowTop  = data.center.y + (height / 2);

        // For each node, add that node at its given position
        (data.connections || []).forEach(function(nodeInfo, i) {
          var newNode = document.createElement("li");
          newNode.classList.add("connection");
          nodeInfo.position = nodeInfo.position || {"x": 0, "y": 0};
          nodeInfo.position.x = nodeInfo.position.x || 0;
          nodeInfo.position.y = nodeInfo.position.y || 0;
          newNode.style.left = nodeInfo.position.x + "px";
          newNode.style.top  = nodeInfo.position.y + "px";

          var icon = document.createElement("div");
          icon.classList.add("icon");
          icon.setAttribute("data-object-type", nodeInfo.type);
          newNode.appendChild(icon);

          var labelType = document.createElement("span");
          labelType.classList.add("type");
          labelType.innerHTML = nodeInfo.type;
          newNode.appendChild(labelType);

          var labelName = document.createElement("span");
          labelName.classList.add("name");
          labelName.innerHTML = nodeInfo.name;
          newNode.appendChild(labelName);

          newNode.setAttribute("data-index", i);
          newNode.setAttribute("data-object-type",     nodeInfo.type);
          newNode.setAttribute("data-object-id",       nodeInfo.id);
          newNode.setAttribute("data-object-revision", nodeInfo.revision);

          var inputs  = document.createElement("ul");
          inputs.classList.add("inputs");
          var outputs = document.createElement("ul");
          outputs.classList.add("outputs");

          // For each wire, add the corresponding wire
          nodeInfo.inputs.forEach(function(pinInfo, j) {
            if (pinInfo.connections.length == 0) {
              var newWire = document.createElement("li");
              newWire.classList.add("input");
              newWire.classList.add("disconnected");
              newWire.setAttribute("data-index", j);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = pinInfo.type;
              label.appendChild(wireLabelType);

              if (pinInfo.type == "configuration") {
                newWire.classList.add("hidden");
                newWire.setAttribute("hidden", true);
              }

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = pinInfo.name;
              label.appendChild(wireLabelName);

              newWire.appendChild(label);
              inputs.appendChild(newWire);
            }
            else {
              pinInfo.connections.forEach(function(wireInfo, k) {
                var newWire = document.createElement("li");
                newWire.classList.add("input");
                newWire.setAttribute("data-index", j);
                newWire.setAttribute("data-item-index", k);

                // Increment all output wire indexes
                wireInfo.to[1] = wireInfo.to[1] + 1;

                newWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

                if (pinInfo.type == "configuration") {
                  newWire.classList.add("hidden");
                  newWire.setAttribute("hidden", true);
                }

                if (k == 0) {
                  var label = document.createElement("div");
                  label.classList.add("label");

                  var wireLabelType = document.createElement("span");
                  wireLabelType.classList.add("type");
                  wireLabelType.innerHTML = pinInfo.type;
                  label.appendChild(wireLabelType);

                  var wireLabelName = document.createElement("span");
                  wireLabelName.classList.add("name");
                  wireLabelName.innerHTML = pinInfo.name;
                  label.appendChild(wireLabelName);

                  newWire.appendChild(label);
                }

                inputs.appendChild(newWire);
              });
            }
          });

          // For each wire, add the corresponding wire
          // First, add the 'self' wire
          nodeInfo["self"] = nodeInfo["self"] || {"connections": []};
          if (nodeInfo["self"].connections.length == 0) {
            var selfWire = document.createElement("li");
            selfWire.classList.add("output");
            selfWire.classList.add("disconnected");

            var label = document.createElement("div");
            label.classList.add("label");

            var wireLabelType = document.createElement("span");
            wireLabelType.classList.add("type");
            wireLabelType.innerHTML = nodeInfo.type;
            label.appendChild(wireLabelType);

            var wireLabelName = document.createElement("span");
            wireLabelName.classList.add("name");
            wireLabelName.innerHTML = "self";
            label.appendChild(wireLabelName);

            selfWire.setAttribute("data-index", "0");

            selfWire.appendChild(label);
            outputs.appendChild(selfWire);

            // If there are other outputs, this wire is hidden by default
            if (nodeInfo.outputs.length > 0) {
              selfWire.setAttribute("hidden", true);
              selfWire.classList.add("hidden");
            }
          }
          else {
            nodeInfo["self"].connections.forEach(function(wireInfo, k) {
              var selfWire = document.createElement("li");
              selfWire.classList.add("output");
              selfWire.classList.add("disconnected");
              selfWire.setAttribute("data-index", "0");
              selfWire.setAttribute("data-item-index", k);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = nodeInfo.type;
              label.appendChild(wireLabelType);

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = "self";
              label.appendChild(wireLabelName);

              selfWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

              selfWire.appendChild(label);
              outputs.appendChild(selfWire);
            });
          }

          nodeInfo.outputs.forEach(function(pinInfo, j) {
            if (pinInfo.connections.length == 0) {
              var newWire = document.createElement("li");
              newWire.classList.add("output");
              newWire.classList.add("disconnected");
              newWire.setAttribute("data-index", j+1);

              var label = document.createElement("div");
              label.classList.add("label");

              var wireLabelType = document.createElement("span");
              wireLabelType.classList.add("type");
              wireLabelType.innerHTML = pinInfo.type;
              label.appendChild(wireLabelType);

              var wireLabelName = document.createElement("span");
              wireLabelName.classList.add("name");
              wireLabelName.innerHTML = pinInfo.name;
              label.appendChild(wireLabelName);

              newWire.appendChild(label);
              outputs.appendChild(newWire);
            }
            else {
              pinInfo.connections.forEach(function(wireInfo, k) {
                var newWire = document.createElement("li");
                newWire.classList.add("output");
                newWire.setAttribute("data-index", j+1);
                newWire.setAttribute("data-item-index", k);
                newWire.setAttribute("data-connected-to", wireInfo.to[0] + "-" + wireInfo.to[1] + "-" + (wireInfo.to[2] || 0));

                if (k == 0) {
                  var label = document.createElement("div");
                  label.classList.add("label");

                  var wireLabelType = document.createElement("span");
                  wireLabelType.classList.add("type");
                  wireLabelType.innerHTML = pinInfo.type;
                  label.appendChild(wireLabelType);

                  var wireLabelName = document.createElement("span");
                  wireLabelName.classList.add("name");
                  wireLabelName.innerHTML = pinInfo.name;
                  label.appendChild(wireLabelName);

                  newWire.appendChild(label);
                }

                outputs.appendChild(newWire);
              });
            }
          });

          newNode.appendChild(inputs);
          newNode.appendChild(outputs);
          self.connections[0].appendChild(newNode);
        });

        self.workflow.reinitialize();
      });
    });
  };

  Workflow.prototype.applySidebarNodeEvents = function(nodes) {
    var self = this;

    nodes.on('mousedown.occam-node-move', function(event) {
      // Adds the node

      var node = $(this);
      var sidebarConnections = node.parents("ul.connections").first();
      var clonedNode = node.clone();

      var startX = 0;
      var startY = 0;
      node.css({
        left: node.offset().left - self.draggable.offset().left,
        top:  node.offset().top  - self.draggable.offset().top,
        "z-index": 99999
      });

      node[0].classList.remove("dummy");
      node[0].setAttribute("data-index", self.workflow.nodes().length);

      self.connections.append(node);
      var workflowNode = window.Workflow.Node.createFor(node[0], self.workflow);
      workflowNode.initialize();
      self.workflow.initializeNode(workflowNode);
      window.Workflow.nodeClickEvent.call(this, event);
      workflowNode.element.style.zIndex = 99999;

      self.element.on('mousemove.occam-node-move', function(event) {
      }).one('mouseup.occam-node.move', function(event) {
        $(this).off('mousemove.occam-node-move');
        node.off('mousedown.occam-node-move');
        node.css({
          "z-index": ""
        });
      });

      sidebarConnections.append(clonedNode);
      self.applySidebarNodeEvents(clonedNode);

      var collapse = self.element.parent().find('.collapse:not(.right)');
      collapse.trigger('click');

      // TODO: move this to Occam.Configuration
      // Add configuration
      self.configurationTabs.addTab(node.children(".name").text(), function(configurationPanel) {
        // TODO: use the correct connection index
        var template = document.querySelector("template#configuration-panel");
        var form = null;

        // FUTURE: remove this test when browsers stabilize
        if ('content' in template) {
          form = document.importNode(template.content, true)
        }
        else {
          form = template.querySelector("form").cloneNode(true);
        }
        configurationPanel.parentNode.appendChild(form);
        form = configurationPanel.parentNode.querySelector("form:last-child");
        configurationPanel.remove();

        var subTabsElement = form.querySelector(".tabs");
        if (subTabsElement) {
          var subTabs = Occam.Tabs.load(subTabsElement);

          if (self.currentObject) {
            self.currentObject.objectInfo(function(objectInfo) {
              // Set up the configuration panel
              (objectInfo.inputs || []).forEach(function(inputInfo, index) {
                if (inputInfo.type == "configuration") {
                  subTabs.addTab(inputInfo.name, function(subPanel) {
                    subPanel.classList.add("configuration");

                    subPanel.setAttribute("data-input-index", index);
                    subPanel.setAttribute("data-object-id", objectInfo.id);
                    subPanel.setAttribute("data-object-revision", self.currentObject.revision);
                    subPanel.setAttribute("data-key", btoa(inputInfo.name));

                    // Load the configuration panel
                    Occam.Configuration.load($(subPanel));
                  });
                }
              });
            });
          }

          subTabs.select(0);
        }
      });

      self.exportJSON();
    });
  };

  Workflow.prototype.exportJSON = function() {
    var self = this;

    // Attach this object to the workflow
    var workflowData = self.workflow.exportJSON();

    // Mutate this into the appropriate structure
    var canonicalData = {};

    canonicalData.connections = [];
    workflowData.connections.forEach(function(nodeData) {
      var canonicalNode = {};

      canonicalNode.inputs = [];
      canonicalNode.position = {
        "x": nodeData.left,
        "y": nodeData.top
      };
      canonicalNode.name     = nodeData.name;
      canonicalNode.type     = nodeData.type;
      canonicalNode.id       = nodeData.data["object-id"]
      canonicalNode.revision = nodeData.data["object-revision"]

      nodeData.inputs.forEach(function(pinData, pinIndex) {
        var canonicalPin = {};
        canonicalPin.connections = [];
        canonicalPin.name = pinData.name;
        canonicalPin.type = pinData.type;
        pinData.wires.forEach(function(wireData) {
          var canonicalWire = {};
          canonicalWire.to = [wireData.toNode, wireData.toWire-1, wireData.toIndex]
          if (canonicalWire.to[0] !== undefined) {
            canonicalPin.connections.push(canonicalWire);
          }
        });
        canonicalNode.inputs.push(canonicalPin);
      });

      canonicalNode.outputs = [];
      nodeData.outputs.forEach(function(pinData, pinIndex) {
        // self pin
        var canonicalPin = {};
        canonicalPin.connections = [];
        canonicalPin.name = pinData.name;
        canonicalPin.type = pinData.type;
        pinData.wires.forEach(function(wireData) {
          var canonicalWire = {};
          canonicalWire.to = [wireData.toNode, wireData.toWire, wireData.toIndex]
          if (canonicalWire.to[0] !== undefined) {
            canonicalPin.connections.push(canonicalWire);
          }
        });
        if(pinIndex == 0){
          canonicalNode.self = canonicalPin;
        }
        else{
          canonicalNode.outputs.push(canonicalPin);
        }
      });

      canonicalData.connections.push(canonicalNode);
    });

    var width  = self.workflow.element.clientWidth;
    var height = self.workflow.element.clientHeight;

    canonicalData.center = workflowData.center;

    console.log(workflowData);
    console.log(JSON.stringify(canonicalData));

    return JSON.stringify(canonicalData);
  };

  /* This function is the event handler for when the "Attach" button is
   * clicked. It should add an "attach" event to the queue.
   */
  Workflow.prototype.submitAttach = function(newActiveBox, container, workflow) {
    // Obviously, name and type might be wrong when somebody types something
    // weird in after selecting an object from the dropdown.

    var form = newActiveBox.find('form');

    // Get the hidden field with the object id requested to add
    var objectId = form.find(".object-id").val().trim();
    var objectRevision = form.find(".object-revision").val().trim();
    var connectionIndex = form.find("input[name=connection_index]").val().trim();
    var objectType = form.find("input[name=object_type]").val().trim();
    var objectName = form.find("input[name=object_name]").val().trim();

    Occam.object.queuePushAttach(connectionIndex, objectId, objectRevision, function(success) {
      console.log("Attach finished: " + success);
    });
  };
};
