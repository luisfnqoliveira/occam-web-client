/* This file handles the social component and comment streams.
 */

// Initialize all selectors on the page
window.addEventListener("load", function() {
  Occam.Social.loadAll(document.body);
});

var initOccamSocial = function(Occam) {
  var Social = Occam.Social = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element = element;
    this.editForm = element.previousElementSibling;
    this.replyForm = this.editForm.previousElementSibling;

    Social.count++;
    this.element.setAttribute('data-social-index', Social.count);

    Social._loaded[this.element.getAttribute('data-social-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Social.count = 0;
  Social._loaded = {};

  Social.loadAll = function(element) {
    var elements = element.querySelectorAll('ul.comments:not(.replies)');

    elements.forEach(function(element) {
      Social.load(element);
    });
  };

  Social.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-social-index');

    if (index) {
      return Occam.Social._loaded[index];
    }

    return new Occam.Social(element);
  };

  Social.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Social.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Social.prototype.bindEvents = function() {
    var self = this;

    // Bind events for every comment
    self.element.querySelectorAll('li.comment').forEach(function(comment) {
      self.bindComment(comment);
    });

    self.replyForm.addEventListener("submit", function(event) {
      event.stopPropagation();
      event.preventDefault();

      // Submit the form
      Occam.Util.submitForm(self.replyForm, function(html) {
        // Add the new comment to the list
        var node = document.createElement("div");
        node.innerHTML = html;
        var newElement = node.querySelector("li.comment");
        self.element.appendChild(newElement);
        self.bindComment(newElement);
      });
    });
  };

  Social.prototype.showReplyBox = function(commentID, element) {
    var self = this;
    if (!element.querySelector(':scope > .comment-body + form')) {
      var newForm = this.replyForm.cloneNode(true);
      newForm.removeAttribute("id");
      newForm.removeAttribute("hidden");
      newForm.classList.add("reply-box");
      newForm.querySelector('input[name="inReplyTo"]').setAttribute("value", commentID);
      element.insertBefore(newForm, element.querySelector('.comment-body + .replies.comments'));
      newForm.classList.add("revealed");

      newForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(newForm, function(html) {
          newForm.remove();
          // Add the new comment to the list
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          var replies = element.querySelector(':scope > .comment-body ~ .replies.comments');
          replies.appendChild(newElement);
          self.bindComment(newElement);
        });
      });
    }
  };

  Social.prototype.showEditBox = function(commentID, element) {
    var self = this;
    if (!element.querySelector(':scope > .comment-body + form')) {
      var newForm = this.editForm.cloneNode(true);
      newForm.removeAttribute("id");
      newForm.removeAttribute("hidden");
      newForm.classList.add("edit-box");
      newForm.setAttribute("action", newForm.getAttribute("action") + commentID);
      element.insertBefore(newForm, element.querySelector('.comment-body + .replies.comments'));
      newForm.classList.add("revealed");
      newForm.querySelector(':scope > textarea').value = element.querySelector('.comment-body > .content').innerText;

      newForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(newForm, function(html) {
          newForm.remove();

          // Update with the new comment
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          element.parentNode.replaceChild(newElement, element);
          self.bindComment(newElement);
        });
      });
    }
  };

  Social.prototype.showMoreReplies = function(commentID, element) {
    var self = this;
    var replies = element.querySelector('.comment-body ~ .replies.comments');
    var link = element.querySelector('.comment-body ~ .replies.comments + span > .show-replies');
    var url = link.getAttribute("href");
    Occam.Util.get(url, function(html) {
      replies.innerHTML = html;
      element.appendChild(replies.firstChild);
      replies.remove();
      link.parentNode.remove();
      element.querySelectorAll('li.comment').forEach(function(comment) {
        self.bindComment(comment);
      });
    });
  };

  Social.prototype.bindComment = function(element) {
    var self = this;

    var commentID = element.querySelector("span.id").textContent;

    // Bind the show more comments link.
    var showMoreLink = element.querySelector(":scope > .comment-body ~ .replies.comments + span > .show-replies");
    if (showMoreLink) {
      showMoreLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showMoreReplies(commentID, element);
      });
    }

    // Bind the 'reply' link
    var replyLink = element.querySelector(":scope > .comment-body > .reply-link");
    if (replyLink) {
      replyLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showReplyBox(commentID, element);
      });
    }

    // Bind the 'edit' link
    var editLink = element.querySelector(":scope > .comment-body > .edit-link");
    if (editLink) {
      editLink.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        self.showEditBox(commentID, element);
      });
    }

    // Bind the 'delete' action
    var deleteForm = element.querySelector("form.delete-form");
    if (deleteForm) {
      deleteForm.addEventListener("submit", function(event) {
        event.stopPropagation();
        event.preventDefault();

        // Submit the form
        Occam.Util.submitForm(deleteForm, function(html) {
          // Add the new comment to the list
          var node = document.createElement("div");
          node.innerHTML = html;
          var newElement = node.querySelector("li.comment");
          element.parentNode.replaceChild(newElement, element);
          self.bindComment(newElement);
        });
      });
    }
  };
};
