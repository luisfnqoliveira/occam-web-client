/* This file handles opening modals on the page.
 */

// Initialize all selectors on the page
window.addEventListener("load", function() {
  Occam.Modal.loadAll(document.body);
});

var initOccamModal = function(Occam) {
  var Modal = Occam.Modal = function(element) {
    var self = this;

    if (element === undefined) {
      return;
    }

    this.element = element;

    if (Modal.element === undefined) {
      Modal.element     = document.querySelector("body > .modal-window");
      Modal.content     = Modal.element.querySelector(":scope > .content");
      Modal.closeButton = Modal.element.querySelector(":scope > .close");
      Modal.element.addEventListener("click", function(event) {
        Modal.close();
      });

      window.addEventListener("keyup",   Modal.handleKeyEvent);
      window.addEventListener("keydown", Modal.handleKeyEvent);

      Modal.content.addEventListener("click", function(event) {
        event.stopPropagation();
      });
    }

    Modal.count++;
    this.element.setAttribute('data-loaded-index', Modal.count);

    Modal._loaded[this.element.getAttribute('data-loaded-index')] = this;

    this.bindEvents();
    this.events = {};
  };

  Modal.count = 0;
  Modal._loaded = {};

  Modal.loadAll = function(element) {
    var modals = element.querySelectorAll('a.modal');

    modals.forEach(function(element) {
      Modal.load(element);
    });
  };

  Modal.load = function(element) {
    if (element === undefined) {
      return null;
    }

    var index = element.getAttribute('data-loaded-index');

    if (index) {
      return Occam.Modal._loaded[index];
    }

    return new Occam.Modal(element);
  };

  Modal.handleKeyEvent = function(event) {
    if (Modal.isOpen()) {
      if (event.key == "Escape" || event.code == "Escape" || event.keyCode == 27) {
        Modal.close();
      }
    }
  };

  Modal.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      this.events[name].call(this, data);
    };
    return this;
  };

  Modal.prototype.on = function(name, callback) {
    if (callback === undefined) {
      return this.events[name];
    }

    this.events[name] = callback;
    return this;
  };

  Modal.prototype.bindEvents = function() {
    var self = this;

    this.element.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      Modal.open(this.getAttribute("href"));
    });
  };

  Modal.isOpen = function() {
    return Modal.element && Modal.element.style.display == "block";
  };

  Modal.open = function(url) {
    Modal.content.style.display = "none";
    Modal.closeButton.style.display = "none";
    Modal.element.style.display = "block";

    // Remove tabs elsewhere
    document.querySelectorAll("*[tabindex], input, select, a").forEach(function(tabElement) {
      // Store the previous values of the tabindex
      // It will ensure that the default value is '0' which means it is navigated
      // in the default order.
      tabElement.setAttribute("data-tabindex", tabElement.getAttribute("tabindex") || "0");

      // Set tabindex to -1 so that it is not able to be tabbed to
      tabElement.setAttribute("tabindex", "-1");
    });

    // Focus on modal
    Modal.element.focus();

    Occam.Util.get(url, function(html) {
      Modal.content.innerHTML = html;
      Modal.content.style.display = "block";
      Modal.closeButton.style.display = "block";
      Modal.content.style.top = (document.body.clientHeight - Modal.content.clientHeight)/2 + "px";
      Modal.element.querySelector(":scope > .close").style.top = ((document.body.clientHeight - Modal.content.clientHeight)/2 + 2) + "px";

      Occam.ObjectTree.loadAll(Modal.content);
    });
  };

  Modal.close = function() {
    // Reset tabs elsewhere
    document.querySelectorAll("*[tabindex], input, select, a").forEach(function(tabElement) {
      // Restore the previously saved tabindexes
      // If, for some reason, that value is not available, this will set it to -1
      tabElement.setAttribute("tabindex", tabElement.getAttribute("data-tabindex") || "-1");
    });

    // Remove modal
    Modal.element.style.display = "none";
  };
};
