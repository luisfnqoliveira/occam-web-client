/* This file handles pulling objects and tasks from other systems.
 */

// Initialize
$(function() {
});

var initOccamImporter = function(Occam) {
  var Importer = Occam.Importer = function() {
  };

  Importer.prototype.lookup = function(uuid_or_url, host) {
    if (uuid_or_url === undefined) {
      throw("Error: Requires UUID or URL as an argument");
    }
    if ((typeof uuid_or_url) !== "string" && !(uuid_or_url instanceof String)) {
      throw("Error: UUID or URL argument must be a string");
    }

    // Check all known hosts
    if (host === undefined) {
    }
    // Check the given host
    else {
    }
  };
};
