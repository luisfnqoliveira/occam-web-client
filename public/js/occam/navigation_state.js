/*
 * This module handles page navigation and back/forward button semantics.
 */

$(function() {
  // Add the callback for when the browser's back/forward button is pressed.
  window.onpopstate = Occam.NavigationState.popState;
});

var initOccamNavigationState = function(Occam) {
  'use strict';

  // Constructor.
  var NavigationState = Occam.NavigationState = function() {
  };

  // The actual history of dynamic page updates
  NavigationState._history = [];
  NavigationState._current = -1;

  /*
   * This method is used to create a new entry in the history. "module" is the
   * string of the Occam javascript module. "index" is used to call that
   * module's load() method to get the instance of that page item. That item
   * will have its updateState method called with the given 'oldData' or
   * 'newData' fields when the back/forward button is pushed. It is
   * responsible for reverting/updating its page state respectively.
   */
  NavigationState.pushState = function(module, index, oldData, newData, newURL, newTitle) {
    var state = {
      'title':  newTitle,
      'href':   newURL,
      'old':    oldData,
      'state':  newData,
      'module': module,
      'index':  index,
      'historyIndex': NavigationState._history.length,
    };

    var newState = jQuery.extend(true, {}, state);
    NavigationState._history.push(newState);

    state['history'] = NavigationState._history;

    window.history.pushState(state, "", newURL);
    NavigationState._current = NavigationState._history.length-1;
  };

  /*
   * Reverses the given state changes to the current page.
   * Generally called internally. Not meant to be called directly.
   */
  NavigationState.revert = function(state) {
    var item = Occam[state.module].load(state.index);
    item.updateState(state.old);
  };

  /*
   * Performs the given state changes to the current page.
   * Generally called internally. Not meant to be called directly.
   */
  NavigationState.perform = function(state) {
    var item = Occam[state.module].load(state.index);
    item.updateState(state.state);
  };

  /*
   * This method will move the page state back one page in history.
   */
  NavigationState.back = function() {
    if (NavigationState._current == -1) {
      return;
    }

    var currentState = NavigationState._history[NavigationState._current];
    NavigationState.revert(currentState);

    NavigationState._current--;
    if (NavigationState._current > -1) {
      var state = NavigationState._history[NavigationState._current];

      NavigationState.perform(state);
    }
    else {
      NavigationState._current = -1;
    }
  };

  /*
   * This method will move the page state forward one page in history.
   */
  NavigationState.forward = function() {
    if (NavigationState._current == NavigationState._history.length-1) {
      return;
    }

    NavigationState._current++;

    if (NavigationState._current < NavigationState._history.length) {
      var state = NavigationState._history[NavigationState._current];
      NavigationState.perform(state);
    }
    else {
      NavigationState._current = NavigationState._history.length - 1;
    }
  };

  /*
   * This method returns the browser's current url. It is window.location.pathname;
   */
  NavigationState.currentLocation = function() {
    return window.location.pathname;
  };

  /*
   * This method returns the path from the current object.
   */
  NavigationState.currentPath = function() {
    var currentURL = NavigationState.currentLocation();

    console.log(currentURL);

    var paths = currentURL.slice(1).split("/");
    var SEPARATORS = ["objects", "groups", "experiments",
                      "metadata", "files", "tree", "output",
                      "run", "view"]

    if (paths[0] == "worksets") {
      paths.shift(); // worksets
      paths.shift(); // uuid

      // Check for revision
      if (SEPARATORS.indexOf(paths[0]) == -1 && paths[0].match(/^[a-z0-9]+$/)) {
        paths.shift();
      }
    }

    if (paths[0] == "objects") {
      paths.shift(); // objects
      paths.shift(); // uuid

      if (SEPARATORS.indexOf(paths[0]) == -1 && paths[0].match(/^[a-z0-9]+$/)) {
        paths.shift();
      }
    }

    return "/" + paths.join("/");
  };

  NavigationState.updateLocation = function(newURL) {
    window.history.replaceState(history.state, "", newURL);
  };

  /*
   * This is the callback that is fired when a person presses the back or
   * forward buttons in their browser. The given event will have the index of
   * the "state" in our history. This function will iterate through the history
   * to recreate the page state.
   */
  NavigationState.popState = function(event) {
    var newIndex = -1;
    if (event === null || event.state === null) {
      // Original page
      console.log('original page');
    }
    else {
      newIndex = event.state.historyIndex;
    }

    if (event && event.state) {
      console.log(event.state);
      NavigationState._history = event.state.history;
      NavigationState._history.push(event.state);
    }

    console.log(NavigationState._current);
    console.log(newIndex);
    if (newIndex === undefined || newIndex >= NavigationState._history.length) {
      // Corrupted or unavailable history

      // Try our best
      if (event && event.state) {
        NavigationState.perform(event.state);
      }
      else {
        window.location.reload()
      }

      return;
    }

    while (NavigationState._current != newIndex) {
      if (newIndex < NavigationState._current) {
        NavigationState.back();
      }
      else {
        NavigationState.forward();
      }
    }
  };
};
