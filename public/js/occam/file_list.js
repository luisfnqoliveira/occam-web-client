/* This file handles the javascript for the object file listings.
 * It also handles file input configuration management for things
 * such as selecting which files to use as input.
 */

// Initialize any directory views
window.addEventListener("load", function(e) {
  Occam.FileList.bind(window.document.body);
});

var initOccamFileList = function(Occam) {
  var FileList = Occam.FileList = function(element) {
    this.element = element;

    this.bindEvents();
  };

  FileList.bind = function(element) {
    var fileLists = element.querySelectorAll('.file-viewer');

    fileLists.forEach(function(subElement) {
      var fileList = new Occam.FileList(subElement);
    });
  };

  // Need a function to load a directory
  FileList.prototype.loadGroup = function(url) {
    var self = this;

    // Remove current file list viewer
    var viewer = this.element.querySelector(".card:not(.viewer):not(.pending)");
    if (viewer) {
      viewer.remove();
    }

    // Show loading graphic
    var pending = this.element.querySelector(".card.pending:not(.viewer)");
    if (pending) {
      pending.removeAttribute("hidden");
    }

    Occam.Util.get(url, function(html) {
      var fakeNode = document.createElement("div");
      fakeNode.innerHTML = html;
      var newCard = fakeNode.querySelector(".card");
      self.element.insertBefore(newCard, self.element.children[0]);
      pending.setAttribute("hidden", true);

      // Rebind the events
      self.bindFileListEvents();
    });
  };

  // Need a function to load a file
  FileList.prototype.loadFile = function(url) {
    var self = this;

    // Remove current viewer
    var viewer = this.element.querySelector(".card.viewer:not(.pending)");
    if (viewer) {
      viewer.remove();
    }

    // Show loading graphic
    var pending = this.element.querySelector(".card.pending.viewer");
    if (pending) {
      pending.removeAttribute("hidden");
    }

    Occam.Util.get(url, function(html) {
      var fakeNode = document.createElement("div");
      fakeNode.innerHTML = html;
      var newCard = fakeNode.querySelector(".card.viewer");
      self.element.appendChild(newCard);
      pending.setAttribute("hidden", true);

      // Apply events on the new card
      var runners = newCard.querySelectorAll('.run-viewer');
      if (runners.length > 0) {
        runners.forEach(function(element) {
          new Occam.Runner(element);
        });
      }
    });
  };

  FileList.prototype.bindEvents = function() {
    this.bindFileListEvents();
  };

  FileList.prototype.bindFileListEvents = function() {
    var self = this;

    // Bind click events on each file row
    var rows = this.element.querySelectorAll("table.file-listing tr td.name a");

    Occam.Tooltip.loadAll(this.element);

    rows.forEach(function(row) {
      row.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        var url = this.getAttribute("href");

        if (this.parentNode.parentNode.classList.contains("group")) {
          self.loadGroup(url);
        }
        else {
          self.loadFile(url);
        }
      });
    });
  };
};
