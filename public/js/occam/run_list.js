/*
 * This module handles the run list panel which lets you select previous runs
 * and bring up information about each, or bring up a queue panel.
 * (See: views/objects/_runner.haml)
 */

var initOccamRunList = function(Occam) {
  'use strict';

  var RunList = Occam.RunList = function(element) {
    var self = this;

    // Initialize events
    self.events = {};

    // Get the collapse bar
    self.collapse = element.parentNode.parentNode.querySelector(":scope > .collapse");

    self.bindCollapseEvents();

    // Get the item template
    self.template = element.parentNode.querySelector("template.run-item");

    self.list = element;

    // Ensure the selected option is the tabindex and the rest are not
    self.select(self.selected());

    // Attach events to each existing list item
    self.list.querySelectorAll(":scope > li").forEach(function(item) {
      self.bindEvents(item);
    });
  };

  /* Fires a callback for the given event.
   */
  RunList.prototype.trigger = function(name, data) {
    if (this.events[name]) {
      var eventInfo = this.events[name];
      eventInfo.callback.call(eventInfo.target, data);
    };
    return this;
  };

  /* Establishes an event callback.
   */
  RunList.prototype.on = function(name, callback, target) {
    if (callback === undefined) {
      return this.events[name];
    }

    if (target === undefined) {
      target = this;
    }

    this.events[name] = {
      "callback": callback,
      "target": target
    };
    return this;
  };

  /* Shows/Hides the run list.
   */
  RunList.prototype.showHideList = function(show) {
    var self = this;
    if (show == true || (show != false && self.collapse.classList.contains("reveal"))) {
      // Show
      self.collapse.classList.remove("reveal");
    }
    else {
      // Hide
      self.collapse.classList.add("reveal");
    }
  };

  /* Attaches events to the collapse bar to show/hide the list.
   */
  RunList.prototype.bindCollapseEvents = function() {
    var self = this;
    self.collapse.addEventListener("click", function(event) {
      self.showHideList();
    });
  };

  /* Attaches events to the given item element.
   */
  RunList.prototype.bindEvents = function(item) {
    var self = this;

    var link = item.querySelector("a");

    link.addEventListener("click", function(event) {
      event.preventDefault();
      event.stopPropagation();

      self.select(getChildIndex(item));
    });

    link.addEventListener("focus", function(event) {
      self.showHideList(true);
    });

    link.addEventListener("keydown", function(event) {
      var toggle = null;
      if (event.keyCode == 38) { // up arrow
        toggle = item.previousElementSibling;
      }
      else if (event.keyCode == 40) { // down arrow
        toggle = item.nextElementSibling;
      }
      else if (event.keyCode == 39 || event.keyCode == 37) { // left/right arrow
        // Unfocus the run list and go to content
        event.preventDefault();
        event.stopPropagation();

        self.trigger("focus", item);

        return;
      }

      if (toggle) {
        var index = window.getChildIndex(toggle);
        self.select(index);
        toggle.querySelector("a").focus();
      }
    });

    var deleteButton = item.querySelector("form .delete");
    if (deleteButton) {
      deleteButton.addEventListener("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        // Trigger an event
        self.trigger('cancel', item);
      });
    }
  };

  /* Selects the given item in the list.
   */
  RunList.prototype.select = function(index_or_element) {
    var item = this.elementFor(index_or_element);

    // De-select the currently selected item
    this.list.querySelectorAll(':scope > .active').forEach(function(child) {
      child.classList.remove('active');
      var link = child.querySelector("a");
      link.setAttribute('tabindex', '-1');
      link.setAttribute('aria-selected', 'false');
    });
    var link = item.querySelector("a");
    item.classList.add('active');
    link.setAttribute('tabindex', '0');
    link.setAttribute('aria-selected', 'true');

    // Trigger an event
    this.trigger('change', item);

    // Return the element
    return item;
  };

  /* Returns the index of the currently selected item.
   */
  RunList.prototype.selected = function() {
    var item = this.list.querySelector(':scope > li.active');

    return getChildIndex(item);
  };

  /* Appends the given item to the run list at the given location.
   */
  RunList.prototype.append = function(runStatus, atIndex) {
    if (atIndex === undefined) {
      atIndex = 1;
    }

    var newItem = null;
    if ('content' in this.template) {
      newItem = document.importNode(this.template.content, true);
      newItem = newItem.querySelector("li");
    }
    else {
      newItem = this.template.querySelector("li").cloneNode(true);
    }

    var lastItem = this.list.querySelector(":scope > li:nth-of-type(" + (atIndex + 1) + ")");
    if (!lastItem) {
      this.list.appendChild(newItem);
    }
    else {
      this.list.insertBefore(newItem, lastItem);
    }

    // Set the class name
    newItem.setAttribute("data-status", runStatus);

    // Attach events
    this.bindEvents(newItem);

    // Return the element representing the item
    return newItem;
  };

  RunList.prototype.replace = function(index_or_element, newEntry) {
    var oldEntry = this.elementFor(index_or_element);
    oldEntry.parentNode.replaceChild(newEntry, oldEntry);
    this.bindEvents(newEntry);
    this.select(newEntry);

    return newEntry;
  };

  /* Removes the item.
   */
  RunList.prototype.remove = function(index_or_element) {
    var element = this.elementFor(index_or_element);

    if (!element) {
      return;
    }

    var index = getChildIndex(element);

    var reselect = this.selected() == index;

    element.remove();

    if (reselect) {
      this.select(index - 1);
    }
  };

  /* Retrieves the list element for the given index.
   *
   * Returns null when the item cannot be found.
   */
  RunList.prototype.elementFor = function(index_or_element) {
    // Presume the passed argument is the element
    var item = index_or_element;

    // However, if we are passed an index,
    if (typeof(index_or_element) == "number") {
      if (index_or_element < 0) {
        index_or_element = 0;
      }
      // Find the item to select
      item = this.list.querySelector(':scope > li:nth-of-type(' + (index_or_element + 1) + ')');
    }

    // If the item does not exist, return null.
    if (!item) {
      return null;
    }

    return item;
  };

  /* Retreives information about the given item in the list.
   */
  RunList.prototype.infoFor = function(index_or_element) {
    var item = this.elementFor(index_or_element);

    // Ready the return object.
    var ret = {};

    // Gather the status
    ret.status = item.getAttribute("data-status");
    if (item.hasAttribute("data-run-id")) {
      ret.runID  = item.getAttribute("data-run-id");
    }
    if (item.hasAttribute("data-job-id")) {
      ret.jobID  = item.getAttribute("data-job-id");
    }
    if (item.hasAttribute("data-viewer-id")) {
      ret.viewerID  = item.getAttribute("data-viewer-id");
    }

    return ret;
  };
}
