"use strict";

var initWorkflowWidgetNode = function(window) {
  var Node = window.Workflow.Node = function(element, workflow) {
    this.element  = element;
    this.workflow = workflow;
    this.type     = "node";

    this.collisionChildren = [];

    this.initialize();
  };

  Node.createFor = function(element, workflow) {
    if (!Array.isArray(element)) {
      element = [element];
    }

    if (element.length == 0) {
      return null;
    }

    element = element[0];

    if (!element.workflowNodeObject) {
      element.workflowNodeObject = new Node(element, workflow);
    }

    return element.workflowNodeObject;
  };

  Node.prototype.initialize = function() {
    var element = this.element;

    this.boundsIndex = 0;
  };

  Node.prototype.hide = function() {
    this.element.setAttribute("hidden", true);
  };

  Node.prototype.show = function() {
    this.element.removeAttribute("hidden");
  };

  Node.prototype.redraw = function() {
    this._bounds = undefined;
    this._left   = undefined;
    this._top    = undefined;

    this._wires      = undefined;
    this.inputWires  = undefined;
    this.outputWires = undefined;

    this.arrangeInputWires();
    this.arrangeOutputWires();
    this.repositionInputs();
    this.repositionOutputs();
  };

  Node.prototype.destroy = function() {
    var self = this;
   
    // Disconnect all wires
    for (var wire of this.outputs()) {
      wire.disconnect();
    }

    for (var wire of this.inputs()) {
      wire.disconnect();
    }

    this.workflow.nodes().forEach(function(node, index) {
      var updateConnection = function(wire) {
        var connectedTo = wire.element.getAttribute('data-connected-to');
        if (connectedTo) {
          var nodeIndex = parseInt(connectedTo.split('-')[0]);
          var wireIndex = parseInt(connectedTo.split('-')[1]);
          var itemIndex = parseInt(connectedTo.split('-')[2]);
          if (nodeIndex > self.index()) {
            wire.element.setAttribute('data-connected-to', (nodeIndex-1) + "-" + wireIndex + "-" + itemIndex);
            wire._connectedNode = undefined;
            wire._connectedWire = undefined;
          }
        }
      };

      node.inputs().forEach(updateConnection);
      node.outputs().forEach(updateConnection);
    })

    this.workflow.nodes().forEach(function(node, index) {
      if(index > self.index()) {
        node.element.setAttribute('data-index', index - 1);
        node._index = undefined;
      }
    })

    // Destroy each DOM node representing the node
    this.element.remove();

    // Delete the node cache
    this.workflow._nodes = undefined;
  };

  Node.prototype.updateJobs = function(jobs) {
    var self = this;

    if (!jobs) {
      return;
    }

    if (!self.jobDonut) {
      self.element.classList.add("tracking-jobs");
      self.jobDonut = new window.JobDonut(self.element, {"count": jobs.length, "jobs": jobs});
    }
    else {
      self.jobDonut.updateJobs(jobs);
    }
  };

  /**
   * Adds a new wire given the wire data.
   * @returns {Wire} The wire object representing this wire.
   */
  Node.prototype.addWire = function(wireData, index, asInput) {
    var self = this;

    var wireElement = document.createElement("<li>");

    wireElement.style.top    = wireData.top;
    wireElement.style.left   = wireData.left;
    wireElement.style.right  = wireData.right;
    wireElement.style.width  = wireData.width;
    wireElement.style.height = wireData.height;

    // Add label
    var labelElement = document.createElement("div");
    labelElement.classList.add('label');

    var typeLabel = document.createElement("span");
    typeLabel.classList.add('type');

    var nameLabel = document.createElement('name');
    nameLabel.classList.add('name');

    typeLabel.textContent = wireData.type;
    nameLabel.textContent = wireData.name;

    labelElement.appendChild(typeLabel);
    labelElement.appendChild(nameLabel);

    wireElement.appendChild(labelElement);

    if (wireData.isDown) {
      wireElement.addClass('down');
    }

    if (wireData.isLeft) {
      wireElement.addClass('left');
    }

    if (wireData.isRight) {
      wireElement.addClass('right');
    }

    if (wireData.toNode !== undefined && wireData.toWire !== undefined) {
      wireElement.setAttribute('data-connected-to', wireData.toNode + "-" + wireData.toWire + "-" + wireData.toIndex);
    }

    wireElement.setAttribute('data-index', index);

    if (asInput) {
      wireElement.classList.add('input');
      var inputs = self.element.querySelector("ul.inputs");
      if (!inputs) {
        // Add input section since node does not have it
        inputs = document.createElement("ul");
        inputs.classList.add("inputs");
        self.element.appendChild(inputs);
      }

      inputs.appendChild(wireElement);
    }
    else {
      wireElement.addClass('output');
      var outputs = self.element.querySelector("ul.outputs");
      if (!outputs) {
        outputs = document.createElement("ul");
        outputs.classList.add("outputs");
        self.element.appendChild(outputs);
      }

      outputs.appendChild(wireElement);
    }

    var wire = Workflow.Wire.createFor([wireElement], self.workflow);
    wire = wire[0];

    wire._connectedNode = undefined;
    wire._connectedWire = undefined;

    var connectedWire = wire.connectedWire();

    if (connectedWire) {
      connectedWire._connectedNode = undefined;
      connectedWire._connectedWire = undefined;
    }

    self.outputWires = undefined;
    self.inputWires  = undefined;

    return wire;
  };

  Node.prototype.is = function(node) {
    return this.element === node.element;
  };

  Node.prototype.index = function(node) {
    return parseInt(this.element.getAttribute('data-index'));
  };

  Node.prototype.bounds = function() {
    this._bounds = {
      left:   this.element.offsetLeft,
      right:  this.element.offsetLeft + this.element.offsetWidth,
      top:    this.element.offsetTop,
      bottom: this.element.offsetTop  + this.element.offsetHeight
    };

    return this._bounds;
  };

  Node.prototype.wires = function() {
    if (!this._wires) {
      this._wires = Workflow.Wire.createFor(this.element.querySelectorAll("li.output, li.input"), this.workflow);
    }

    return this._wires;
  };

  Node.prototype.outputs = function() {
    if (!this.outputWires) {
      this.outputWires = Workflow.Wire.createFor(this.element.querySelectorAll("ul.outputs li.output"), this.workflow);
    }

    return this.outputWires;
  };

  Node.prototype.inputs = function() {
    if (!this.inputWires) {
      this.inputWires = Workflow.Wire.createFor(this.element.querySelectorAll("ul.inputs li.input"), this.workflow);
    }

    return this.inputWires;
  };

  Node.prototype.hiddenWireCount = function(wires) {
    var wireCount = 0;
    var last = -1;

    wires.forEach(function(wire) {
      var index = parseInt(wire.element.getAttribute("data-index"));
      if (index > last) {
        if (wire.element.classList.contains("hidden")) {
          last = index;
          wireCount++;
        }
      }
    });

    return wireCount;
  };

  Node.prototype.hiddenInputCount = function() {
    return this.hiddenWireCount(this.inputs());
  };

  Node.prototype.hiddenOutputCount = function() {
    return this.hiddenWireCount(this.outputs());
  };

  /* Arranges the input wires along the edge of the node
   */
  Node.prototype.arrangeInputWires = function() {
    this.inputs().forEach(function(inputWire) {
      inputWire.reposition();
    });
  };

  /* Arranges the output wires along the edge of the node
   */
  Node.prototype.arrangeOutputWires = function() {
    this.outputs().forEach(function(outputWire) {
      outputWire.reposition();
    });
  };

  /* Repositions disconnected output wires.
   */
  Node.prototype.repositionOutputs = function() {
    this.outputs().forEach(function(outputWire) {
      if (!outputWire.isConnected()) {
        // Disconnected
        outputWire.element.classList.add("disconnected");
        var wireLabel = outputWire.label();
        // TODO: make all disconnected inputs the same width when possible
        // taking into account labels that don't fit.
        outputWire.element.style.width = wireLabel.offsetWidth + 15 + "px";
      }
      else {
        outputWire.connectedWire().recalculatePosition();
      }

      if (outputWire.label()) {
        if (!(outputWire.element.classList.contains("left"))) {
          if (outputWire.label().offsetWidth > outputWire.measuredWidth()) {
            outputWire.element.classList.remove("shown");
          }
          else {
            outputWire.element.classList.add("shown");
          }
        }
        else {
          outputWire.element.classList.add("shown");
        }
      }
    });
    this.workflow.createQuadtree();
  };

  Node.prototype.repositionInputs = function() {
    var wires = this.inputs();
    this.workflow.createQuadtree();

    wires.forEach(function(inputWire) {
      if (inputWire.isConnected()) {
        inputWire.recalculatePosition();
      }
      else {
        inputWire.collapse();
      }
    });

    this.workflow.createQuadtree();
    this.repositionDisconnectedWires();
    this.workflow.createQuadtree();
  };

  Node.prototype.repositionDisconnectedWires = function() {
    var self = this;
    var currentX = self.left();
    var currentY = self.top();

    this.wires().forEach(function(wire) {
      if (!wire.isConnected()) {
        // Disconnected Wire
        wire.collapse();
      }
    });
  };

  Node.prototype.width = function() {
    this._width = this.element.offsetWidth;
    return this._width;
  };

  Node.prototype.height = function() {
    this._height = this.element.offsetHeight;
    return this._height;
  };

  Node.prototype.left = function() {
    this._left = this.element.offsetLeft;
    return this._left;
  };

  Node.prototype.top = function() {
    this._top = this.element.offsetTop;
    return this._top;
  };

  Node.prototype.nameLabel = function() {
    return this.element.querySelector(':scope > .name').textContent;
  };

  Node.prototype.typeLabel = function() {
    return this.element.querySelector(':scope > .type').textContent;
  };
};
