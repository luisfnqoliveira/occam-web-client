'use strict';

/**
 * Draws a bar graph in the specified div selector.
 * @param {String} options.selector The css selector pointing to the div to
 * append the graph.
 * @param {number} options.margin.left The left margin in pixels for the graph.
 * @param {number} options.margin.right The right margin in pixels for the graph.
 * @param {number} options.margin.top The top margin in pixels for the graph.
 * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
 * @param {number} options.width The width in pixels of the graph.
 * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
 * @param {String} options.labelx.y The text for the y axis.
 * @param {String} options.colors The color (described as CSS) to use for
 * the bars. This is an array when there are multiple datapoints per x.
 * @param {Array} options.data.groups The array of datapoints.
 */

/**
 * This method will sanitize a graph options hash
 * to fill in the hash with default values.
 */
var sanitizeOptions = function(options) {
  options = options || {};
  options = $.extend(true, {}, options)

  // Initialize Data Section
  options.data     = options.data     || {};

  // Sanitize data
  if (options.data instanceof Array) {
    var array = options.data;
    options.data = {};
    options.data.groups = [{"series": array}];
  }
  else {
    options.data.groups = options.data.groups || [];
  }

  // Default height/width
  options.width    = options.width    || 800;
  options.height   = options.height   || 400;

  // Default Margins
  options.margin   = options.margin   || {};

  // Configuration
  options.configuration = options.configuration || {};
  options.configuration.General =
    options.configuration.General || {};

  var configuration = options.configuration.General;

  // Default Domain Metadata
  configuration.domains = configuration.domains  || {};

  // Set up domains
  var maxPoint = d3.max(options.data.groups, function( d ) {
    return d3.max(d.series);
  });

  var minPoint = d3.min(options.data.groups, function( d ) {
    return d3.min(d.series);
  });

  var minX = 0;
  var maxX = d3.max([configuration.domains.x.series.length - 1,
                     d3.max(options.data.groups, function(d) {
                        return d.series.length - 1;
                     })
                    ]);

  var minGroups = 0;
  var maxGroups = options.data.groups.length;

  if (configuration.domains.x.min === undefined) {
    configuration.domains.x.min = minX;
  }
  if (configuration.domains.x.max === undefined) {
    configuration.domains.x.max = maxX;
  }
  if (configuration.domains.y.min === undefined) {
    configuration.domains.y.min = d3.min([0, minPoint]);
  }
  if (configuration.domains.y.max === undefined) {
    configuration.domains.y.max = maxPoint;
  }

  options.data.groups.forEach(function(d, i) {
    if (d.configuration === undefined) {
      d.configuration = {};
    }
    if (d.configuration.color === undefined) {
      d.configuration.color = "hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)";
    }
  });

  options.margin.top    = options.margin.top    || 20;
  options.margin.left   = options.margin.left   || 20;
  options.margin.right  = options.margin.right  || 20;
  options.margin.bottom = options.margin.bottom || 20;

  return options;
};

var drawBars = function(options) {
  // Sanitize Data
  options = sanitizeOptions(options);
  var aspectHeight = options.width / 3 * 2;
  if (aspectHeight > options.height) {
    var aspectWidth = options.height / 2 * 3;
    options.width = aspectWidth;
  }
  else {
    options.height = aspectHeight;
  }

  var configuration = options.configuration.General;

  // Set up domains
  var minX = 0;
  var maxX = d3.max([configuration.domains.x.series.length - 1,
                     d3.max(options.data.groups, function(d) {
                        return d.series.length - 1;
                     })
                    ]);

  var minGroups = 0;
  var maxGroups = options.data.groups.length;

  var chart_attr = {
    margin: {
      top:    options.margin.top,
      left:   options.margin.left + 50,
      right:  options.margin.right,
      bottom: options.margin.bottom + (25 * (maxGroups+3) / 3) + 20
    }
  };

  chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
  chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;
  options.selector = "#graph-1";

  var svg = d3.select(options.selector)
    .style({
      "background-color": configuration.backgroundColor
    })
    .append("svg:svg")
    .attr("class", "chart")
    .attr('preserveAspectRatio', 'xMidYMid')
    .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
    .attr('width',  options.width  + 'px')
    .attr('height', options.height + 'px');

  var chart = svg.append("svg:g")
                 .attr('transform', 'translate(' + chart_attr.margin.left + ', ' + chart_attr.margin.top + ')');

  var popup = svg.append('g')
                 .attr('visibility', 'hidden');

  popup.append('rect')
       .attr('height', '1.2em')
       .attr('x', 0)
       .attr('y', 0)
       .attr('width', '100px')
       .attr('fill', 'red');
  popup.append('text')
       .text('foo')
       .attr('x', 50)
       .attr('y', '1.0em')
       .attr('fill', 'white')
       .style({
         "text-anchor": "middle"
       });

  var x_series = d3.scale.ordinal()
                   .domain(configuration.domains.x.series)
                   .rangeBands([0, chart_attr.width]);

  var y = d3.scale.linear()
            .domain([configuration.domains.y.max, configuration.domains.y.min])
            .rangeRound([0, chart_attr.height]);

  // Bars
  var seriesSpace   = chart_attr.width / (maxX+1);
  var seriesPadding = seriesSpace * 0.1;
  var seriesWidth   = seriesSpace - (seriesPadding * 2);

  var barSpace      = seriesWidth / options.data.groups.length;
  var barPadding    = barSpace * 0.1;
  var barWidth      = barSpace - (barPadding * 2);

  options.data.groups.forEach(function(e, i) {
    e.group_index = i;
  });

  var draw_bars = function() {
    // Form a new array selecting only groups that are visible
    var data_array = options.data.groups.filter(function(element) {
      return element.visible == undefined || element.visible;
    });

    seriesSpace   = chart_attr.width / (maxX+1);
    seriesPadding = seriesSpace * 0.1;
    seriesWidth   = seriesSpace - (seriesPadding * 2);

    barSpace      = seriesWidth / data_array.length;
    barPadding    = barSpace * 0.1;
    barWidth      = barSpace - (barPadding * 2);

    var bars = chart.append('g')
                    .attr('class', 'bars');

    bars.selectAll('g')
      .data(d3.range(minX, maxX+1))      // For every x axis entry
      .enter().append('svg:g')     // Create a group
        .attr('class', 'group')
        .attr('transform', function(d, i) {
          return 'translate(' + (seriesSpace * i) + ', 0)';
        })
        .selectAll('rect')
        .data(data_array) // For every group
        .enter().append('rect')    // Create a rectangle
          .attr('x', function(d, i) {
            return seriesPadding + barSpace * i + barPadding;
          })
          .attr('y', function(d, i, j) {
            // i is the index of the group we are in
            // j is the index of our x-axis position
            return y(d.series[j]) + 0.5;
          })
          .attr('data-group-index', function(d, i) { return d.group_index; })
          .attr('data-series-index', function(d, i, j) { return j; })
          .attr('width', barWidth)
          .attr('height', function(d, i, j) {
            return chart_attr.height - y(d.series[j]);
          })
          .style({
            fill: function(d, i) {
              return d.configuration.color;
            },
            stroke: function(d, i) {
              return d.configuration.strokeColor;
            }
          });

    $(options.selector + " svg g.bars g.group rect")
      .on('mouseenter', function(e) {
        // Update the text
        var group_index = parseInt($(this).data("group-index"));
        var series_index = parseInt($(this).data("series-index"));
        popup.select('text').text(options.data.groups[group_index].series[series_index]);
        // Measure the width of the text node
        var text_width = popup.select('text').node().getComputedTextLength();
        // Update the width the width of the rectangle
        var width = text_width + 20;
        var text_x = width / 2;
        popup.select('text').attr('x', text_x);
        popup.select('rect').attr('width', width);
        // Move the group into place
        var graph_position = $(options.selector).find('svg').position();
        var offsetX = e.pageX - graph_position.left;
        var offsetY = e.pageY - graph_position.top;
        popup.attr('transform', 'translate(' + (offsetX - text_x) + ',' + (offsetY - 40) + ')');
        popup.attr('visibility', 'visible');
        d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
          .style({
            "stroke":       "black",
            "stroke-width": "3px"
          });
      })
      .on('mouseout', function(e) {
        popup.attr('visibility', 'hidden');
        d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
          .style({
            "stroke":       "",
            "stroke-width": ""
          });
      })
      .on('mousemove', function(e) {
        var text_node = popup.select('text');
        var text_x = parseInt(text_node.attr('x'));
        var graph_position = $(options.selector).find('svg').position();
        var offsetX = e.pageX - graph_position.left;
        var offsetY = e.pageY - graph_position.top;
        popup.attr('transform', 'translate(' + (offsetX - text_x) + ',' + (offsetY - 40) + ')');
      })
      .on('click', function(e) {
        $(options.selector + ' svg g.bars').remove();
        var group_index = parseInt($(this).data("group-index"));
        options.data.groups[group_index].visible = false;
        draw_bars();
      });
  };

  draw_bars();

  // Axis
  var xAxis = d3.svg.axis()
    .scale(x_series)
    .tickSize(6, 3, 1)
    .tickValues(configuration.domains.x.series);

  chart.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0, ' + chart_attr.height + ')')
    .call(xAxis)
    .append("text")
      .attr('class', 'x axis-label')
      .style({
        "text-anchor": "middle",
        "fill": configuration.domains.x.textColor,
      })
      .attr("transform", 'translate('+ chart_attr.width/2+', 38)')
      .text(configuration.domains.x.label);

  var yAxis = d3.svg.axis()
    .scale(y)
    .tickSize(6, 3, 1)
    .tickValues(d3.range(configuration.domains.y.min, configuration.domains.y.max+1, configuration.domains.y.majorStep))
    .orient('left');

  var yAxisG = chart.append('g')
                .attr('class', 'y axis')
                .call(yAxis);

  yAxisG.append("text")
        .attr('class', 'y axis-label')
        .style({
          "text-anchor": "end",
          "fill": configuration.domains.y.textColor,
        })
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .text(configuration.domains.y.label);

  /* Minor ticks */
  if (configuration.domains.y.minorStep > 0) {
    yAxisG.selectAll('line')
          .data(d3.range(configuration.domains.y.min, configuration.domains.y.max+1, configuration.domains.y.minorStep), function(d) { return d;})
          .enter()
          .append('line')
          .attr('class', 'minor')
          .attr('y1', y)
          .attr('y2', y)
          .attr('x1', 0)
          .attr('x2', -10)
          .style({
            "stroke-width": "1px",
            "stroke": configuration.domains.y.minorTickColor
          });
  }

  var legend = chart.selectAll(".legend")
    .data(options.data.groups)
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function(d, i) {
      var x = (chart_attr.width/3) * (i % 3) + seriesPadding + barPadding;
      var y = chart_attr.height + 30 + (25*Math.floor((i+3)/3));
      return "translate(" + x + ", " + y + ")";
    });

  legend.append("text")
    .attr("dy", ".35em")
    .attr("transform", "translate(23,0)")
    .style({
      "text-anchor": "start",
      "fill": configuration.textColor,
    })
    .text(function(d) { return d.configuration.name; });

  legend.append("rect")
    .attr("class", "legend")
    .attr("data-group-index", function(d, i) { return i; })
    .attr("transform", "translate(0,-9)")
    .attr("width", 18)
    .attr("height", 18)
    .style({
      "fill": function(d) {
        return d.configuration.color;
      },
      "stroke": function(d) {
        return d.configuration.strokeColor;
      },
    });

  svg.selectAll('.y.axis .tick line')
    .style({
      "stroke": configuration.domains.y.tickColor
    });

  svg.selectAll('.y.axis .domain')
    .style({
      "fill": configuration.domains.y.color
    });

  svg.selectAll('.y.axis .tick text')
    .style({
      "fill": configuration.domains.y.textColor
    });

  svg.selectAll('.x.axis .tick line')
    .style({
      "stroke": configuration.domains.x.tickColor
    });

  svg.selectAll('.x.axis .domain')
    .style({
      "fill": configuration.domains.x.color
    });

  svg.selectAll('.x.axis .tick text')
    .style({
      "fill": configuration.domains.x.textColor
    });
};
