# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam < Sinatra::Base
  # This handles logins and logouts

  # Retrieve login form
  get '/login' do
    render :haml, :"sessions/login", :layout => !request.xhr?, :locals => {:errors => nil}
  end

  # Sign on
  post '/login' do
    account = login(params["username"], params["password"])

    if account.nil?
      # Error logging in

      errors = nil
      render :haml, :"sessions/login", :layout => !request.xhr?, :locals => {
        :errors => errors
      }
    else
      # Redirect to their personal page
      person_uuid = account.person.uuid
      redirect "/people/#{person_uuid}"
    end
  end

  # Sign out
  get '/logout' do
    logout

    redirect '/'
  end
end
