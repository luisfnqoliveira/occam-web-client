class Occam
  # Allow dynamic colors for any arbitrary svg files
  # TODO: shouldn't assume <?xml ... ?> is the first line
  def recolor_svg()
    content_type = "image/svg+xml"
    forever_cache

    params["name"] = params["splat"].join
    puts params["name"]

    if params["hex"]
      params["color"] = "##{params["hex"]}"
    elsif params["hue"] and params["sat"] and params["light"]
      params["color"] = "hsl(#{params["hue"]}, #{params["sat"]}%, #{params["light"]}%)"
    end

    if params["color"]
      require 'base64'

      headers 'Content-Type' => "image/svg+xml"
      css = "path, rect, circle { fill: #{params["color"]} !important; stroke: transparent !important }"
      embed = Base64.encode64(css)

      stream do |out|
        File.open("public/images/#{params[:name]}.svg") do |f|
          out << f.readline
          out << "<?xml-stylesheet type=\"text/css\" href=\"data:text/css;charset=utf-8;base64,#{embed}\" ?>"
          out << f.read
        end
      end
    else
      send_file "public/images/#{params[:name]}.svg"
    end
  end

  get "/images/dynamic/hue/:hue/sat/:sat/light/:light/*.svg" do
    recolor_svg()
  end

  get "/images/dynamic/hex/:hex/*.svg" do
    recolor_svg()
  end

  get "/images/dynamic/color/:color/*.svg" do
    recolor_svg()
  end
end
