# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2017 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  module ContentHelpers
    def header(options = {})
      options[:alt_text] ||= I18n.t('alt-text.help')
      hidden = !((params["help"].to_s == options[:id].to_s) || options[:reveal])

      "<h2 #{options[:offscreen] ? "class=\"offscreen\" " : ""}id=#{options[:id]}>#{options[:text]}" +
        (options[:offscreen] ? "</h2>" : "") +
        (options[:help] ? "<button aria-expanded=\"false\" class=\"help-bubble inline\" href=\"?help=#{options[:id]}\">#{options[:alt_text]}</button>" : "") +
        (options[:offscreen] ? "" : "</h2>") +
        (options[:help] ? "<div class=\"help\" #{hidden ? "hidden" : ""}>" +
                           render(:markdown, options[:help]) +
                           "</div>" : "")
    end
  end

  helpers ContentHelpers
end
