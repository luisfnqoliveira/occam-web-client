require_relative "helper"

describe Occam::ResultsHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::ResultsHelpers
  end

  require 'nokogiri'
  require 'base64'

  TEST_HTML_UNSAFE_SCHEMA = {
    :"foo<ul>b</ul>ar" => {
      :type => "int"
    }
  }

  TEST_HTML_UNSAFE_ARRAY_SCHEMA = {
    :"foo<ul>b</ul>ar" => [{
      :type => "int"
    }]
  }

  TEST_INT_ITEM_SCHEMA = {
    :foo => {
      :units => "seconds",
      :type  => "int"
    }
  }

  TEST_INT_ITEM_ARRAY_SCHEMA = {
    :foo => [{
      :units => "seconds",
      :type  => "int"
    }]
  }

  TEST_INT_ITEM_WITHOUT_UNITS_SCHEMA = {
    :foo => {
      :type  => "int"
    }
  }

  TEST_OUTPUT_SCHEMA = {
    :foo => {
      :units => "seconds",
      :type  => "int"
    },
    :baz => [{
      :units => "watts",
      :type  => "int"
    }]
  }

  before do
    @app = HelperTest.new
  end

  describe "render_data" do
    it "should return a string" do
      @app.render_data({}, {}).must_be_instance_of String
    end

    it "should render to a <ul class='hash'> element when given an hash" do
      result = @app.render_data({}, {})
      Nokogiri::XML.parse(result).xpath("/*[contains(@class,'hash')]").first.name.must_equal "ul"
    end

    it "should render an integer value when given one" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'value')]").first.text.must_equal "43"
    end

    it "should render an <li> for the value with the appropriate id" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//li[contains(@data-id,'data-#{Base64.urlsafe_encode64("foo")}')]/span[contains(@class,'value')]").first.wont_be_nil
    end

    it "should render the key along with the data" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'key')]").first.text.must_equal "foo"
    end

    it "should render the key while escaping HTML" do
      result = @app.render_data({:"foo<ul>b</ul>ar" => 43}, TEST_HTML_UNSAFE_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'key')]").first.text.must_equal "foo<ul>b</ul>ar"
    end

    it "should render the key of an array while escaping HTML" do
      result = @app.render_data({:"foo<ul>b</ul>ar" => [43]}, TEST_HTML_UNSAFE_ARRAY_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'key')]").first.text.must_equal "foo<ul>b</ul>ar"
    end

    it "should render the urlsafe base64 of the key in the data-key attribute" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_SCHEMA)
      key = Nokogiri::XML.parse(result).xpath("//span[contains(@class,'key')]").first.attribute("data-key").value
      Base64.urlsafe_decode64(key).must_equal "foo"
    end

    it "should render the urlsafe base64 of the key of an array in the data-key attribute" do
      result = @app.render_data({:foo => [43]}, TEST_INT_ITEM_ARRAY_SCHEMA)
      key = Nokogiri::XML.parse(result).xpath("//span[contains(@class,'key')]").first.attribute("data-key").value
      Base64.urlsafe_decode64(key).must_equal "foo"
    end

    it "should render the units in the data-units attribute" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'value')]").first.attribute("data-units").value.must_equal "seconds"
    end

    it "should not render the units in the data-units attribute when none are given" do
      result = @app.render_data({:foo => 43}, TEST_INT_ITEM_WITHOUT_UNITS_SCHEMA)
      Nokogiri::XML.parse(result).xpath("//span[contains(@class,'value')]").first.attribute("data-units").must_be_nil
    end

    it "should render arrays after non-array items when given such a schema" do
      result = @app.render_data({:foo => 43, :baz => [44,23,5]}, TEST_OUTPUT_SCHEMA)
      Nokogiri::XML.parse(result).xpath("/*[contains(@class,'hash')]").first.name.must_equal "ul"
    end

    it "should render to a <ul class='array'> element when given an array" do
      result = @app.render_data([], [])
      Nokogiri::XML.parse(result).xpath("/*[contains(@class,'array')]").first.name.must_equal "ul"
    end

    it "should return the string itself when given a string" do
      result = @app.render_data(:foo, {})
      result.must_equal "foo"
    end

    it "should return valid HTML when given an array" do
      result = @app.render_data([], [])
      Nokogiri::HTML.parse(result).errors.empty?.must_equal true
    end

    it "should return valid HTML when given a hash" do
      result = @app.render_data({}, {})
      Nokogiri::HTML.parse(result).errors.empty?.must_equal true
    end
  end
end
